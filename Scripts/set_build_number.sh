#!/bin/sh

set -e
#The set -e command makes a bash script fail immediately when any command returns an non-zero exit code.

if test -z $1
then
    configuration="Debug"
else
    configuration=$1
fi

if [ "$configuration" != "Release" ]; then
    git=$(sh /etc/profile; which git)
    number_of_commits=$("$git" rev-list HEAD --count)
    git_release_version=$("$git" describe --tags --always --abbrev=0)
    if [ -n "$ipaPath" ]; then
        plist="$ipaPath/Payload/$appname-$configuration/Info.plist"
        /usr/libexec/PlistBuddy -c "Set :CFBundleVersion $number_of_commits" "$plist"
    else
        target_plist="$TARGET_BUILD_DIR/$INFOPLIST_PATH"
        dsym_plist="$DWARF_DSYM_FOLDER_PATH/$DWARF_DSYM_FILE_NAME/Contents/Info.plist"

        echo "### BundleVersion = $number_of_commits, shortVersion = $git_release_version"

        for plist in "$target_plist" "$dsym_plist";
        do
            if [ -f "$plist" ]; then
                /usr/libexec/PlistBuddy -c "Set :CFBundleVersion $number_of_commits" "$plist"
                /usr/libexec/PlistBuddy -c "Set :CFBundleShortVersionString ${git_release_version#*v}" "$plist"
            fi
        done
    fi
fi
