#!/bin/sh

set -e
#The set -e command makes a bash script fail immediately when any command returns an non-zero exit code.

if test -z $1
then
    configuration="Debug"
else
    configuration=$1
fi

export appname="MVVM_Demo"
sdk="iphoneos"
certificate="iPhone Developer: Trung Pham (W855FUQL4T)"
build_location="$PWD/build"

if [ ! -d "$build_location" ];
then
mkdir -p "$build_location"
fi
echo "$build_location"

if [[ ${configuration} != *"Release" ]];
then
    profileName="BondClub_dev"
    displayname="$appname-$configuration"
    if [[ ${configuration} == *"Adhoc" ]];
    then
        profileName="Appy_Adhoc"
        certificate="iPhone Distribution: Trung Pham (Q8BBN8EW2D)"
    fi
else
    profileName="Appy_Distribution"
    displayname="$appname"
    certificate="iPhone Distribution: Trung Pham (Q8BBN8EW2D)"
fi

# Set the bundle id.
#echo "Setting bundle name to '$displayname'"
#/usr/libexec/plistbuddy -c "Set :CFBundleName $displayname" "$PWD/$appname/Info.plist"

echo "=====We now clean the project====="
xcodebuild -project $appname.xcodeproj -scheme $appname -configuration $configuration clean

echo "=====We now build the archive====="

# I. Build base on default xcode setting
#xcodebuild -project $appname.xcodeproj -scheme $appname -sdk iphoneos -configuration $configuration archive -archivePath $build_location/$configuration/$displayname.xcarchive

# II. build after modify environment variables (2 ways)
# 1. Via xcconfig
xcconfigPath="$PWD/Scripts/xcconfigs$configuration.xcconfig"
xcodebuild -project $appname.xcodeproj -scheme $appname -sdk iphoneos -configuration $configuration -xcconfig "$xcconfigPath" archive -archivePath $build_location/$configuration/$displayname.xcarchive
# 2. via environment variable in command line
#PROFILE_FILE_NAME="$PWD/Profiles/$configuration.mobileprovision"
#uuid=`/usr/libexec/plistbuddy -c Print:UUID /dev/stdin <<< \
#\`security cms -D -i ${PROFILE_FILE_NAME}\``
#echo "Found UUID $uuid"
#output="$HOME/Library/MobileDevice/Provisioning Profiles/$uuid.mobileprovision"
#cp "$PROFILE_FILE_NAME" "$output"
#xcodebuild -project $appname.xcodeproj -scheme $appname -sdk iphoneos -configuration $configuration CODE_SIGN_IDENTITY="$certificate" PROVISIONING_PROFILE=$uuid PROVISIONING_PROFILE_SPECIFIER="$profileName" archive -archivePath $build_location/$configuration/$displayname.xcarchive

echo "=====We now export to the ipa====="
xcodebuild -exportArchive -archivePath $build_location/$configuration/$displayname.xcarchive -exportOptionsPlist $PWD/Scripts/$configuration.plist -exportPath $build_location/$configuration

echo "====== Upload package to Fabric ======"
export ipaPath=$build_location/$configuration/$appname.ipa
./Scripts/upload-to-crashlytics.sh
