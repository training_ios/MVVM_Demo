#!/bin/bash
set -e

# This scripts allows you to upload a binary to the Crashlytic

crashlytics_api_key="de14c5baed98438f883b55fdd163923cce052ca2"
crashlytics_build_secret="ada70d0c88074d5ee8b95e10062a2cdc7f0b0787492cad72b5bd2554acd67fd6"

echo "$ipaPath"
echo "$crashlytics_build_secret"

./$appname/Frameworks/Crashlytics.framework/submit "$crashlytics_api_key" "$crashlytics_build_secret" -ipaPath "$ipaPath" -notesPath "Scripts/ReleaseNotes.txt" -groupAliases test -notifications YES
