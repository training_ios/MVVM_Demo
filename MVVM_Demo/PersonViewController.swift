//
//  PersonViewController.swift
//  MVVM_Demo
//
//  Created by LeNgocDuy on 4/19/16.
//  Copyright © 2016 Le Ngoc Duy. All rights reserved.
//

import UIKit

class PersonViewController: UIViewController {

	@IBOutlet var nameLabel: UILabel!
	@IBOutlet var birthdateLabel: UILabel!
	fileprivate let viewModel = PersonViewModel(person: Person(salutation: "Dr.", firstName: "first", lastName: "last", birthdate: Date(timeIntervalSince1970: 0)))
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		nameLabel.text = viewModel.fullText
		birthdateLabel.text = viewModel.birthdateText
	}


}

