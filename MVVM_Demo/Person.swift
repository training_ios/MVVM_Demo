//
//  Person.swift
//  MVVM_Demo
//
//  Created by LeNgocDuy on 4/19/16.
//  Copyright © 2016 Le Ngoc Duy. All rights reserved.
//

import Foundation

class Person {
	var salutation: String
	var firstName: String
	var lastName: String
	var birthdate: Date
	
	init(salutation: String, firstName: String, lastName: String, birthdate: Date) {
		self.salutation = salutation
		self.firstName = firstName
		self.lastName = lastName
		self.birthdate = birthdate
	}
}
