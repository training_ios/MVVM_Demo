//
//  PersonViewModel.swift
//  MVVM_Demo
//
//  Created by LeNgocDuy on 4/19/16.
//  Copyright © 2016 Le Ngoc Duy. All rights reserved.
//

import Foundation

class PersonViewModel {
	
	fileprivate var person: Person
	var nameText: String
    var fullText: String
	var birthdateText: String
	
	init(person: Person) {
		self.person = person
		
		if (person.salutation.lengthOfBytes(using: String.Encoding.utf8) > 0) {
			nameText = person.salutation + " " + person.firstName + " " + person.lastName
		} else {
			nameText = person.firstName + " " + person.lastName
		}
		
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "EEEE MMMM d, yyyy"
		birthdateText = dateFormatter.string(from: person.birthdate as Date)
        fullText = person.firstName + " " + person.lastName
	}
}
