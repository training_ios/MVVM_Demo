//
//  MVVM_DemoTests.swift
//  MVVM_DemoTests
//
//  Created by LeNgocDuy on 4/19/16.
//  Copyright © 2016 Le Ngoc Duy. All rights reserved.
//

import XCTest
@testable import MVVM_Demo

class MVVM_DemoTests: XCTestCase {
	
	let salutation = "Dr."
	let firstName = "first"
	let lastName = "last"
	let birthdate = Date(timeIntervalSince1970: 0)
	
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.

    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
	
	
	func testExample() {
		// This is an example of a functional test case.
		// Use XCTAssert and related functions to verify your tests produce the correct results.
	}
	
	func testPerformanceExample() {
		// This is an example of a performance test case.
		self.measure {
			// Put the code you want to measure the time of here.
		}
	}
	
	func testSalutationValid() {
		let person = Person(salutation: salutation, firstName: firstName, lastName: lastName, birthdate: birthdate)
		let model = PersonViewModel(person: person)
		XCTAssert(model.nameText == "Dr. first last", "should use the salutation available.")
	}
	
	func testSalutationInValid() {
		let person = Person(salutation: "", firstName: firstName, lastName: lastName, birthdate: birthdate)
		let model = PersonViewModel(person: person)
		XCTAssert(model.nameText == "first last", "should not use an unavailable salutation.")
	}
	
	func testCorrectFormat() {
		let person = Person(salutation: "", firstName: firstName, lastName: lastName, birthdate: birthdate)
		let model = PersonViewModel(person: person)
		XCTAssert(model.birthdateText == "Thursday January 1, 1970", "should use the correct date format.")
	}
	
}
